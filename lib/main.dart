import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        body: Container(
          width: double.infinity,
          height: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Colors.blue,
                Colors.deepPurple
              ]
            )
          ),
          child: SafeArea(
              child: Column(
                children: [
                  CircleAvatar(
                    radius: 75,
                    foregroundImage: AssetImage('images/1.jpg'),
                  ),
                  Text('Ameer Abo AlShar',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w700
                  ),
                  ),
                  Text('Full Stack Developer',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.w500
                  ),
                  ),
                  info(Icon(Icons.phone),'+963 934 801 138'),
                  info(Icon(Icons.email),'ameer.mamoon@gmail.com'),
                ],
              )
          ),
        )
      )
    );
  }

  Widget info(Widget icon,String text){
    return Container(
      width: 260,
      height: 40,
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(7.5)
      ),
      child: Row(
        children: [
          icon,
          SizedBox(width: 10,),
          Text(text)
        ],
      ),
    );
  }

}
